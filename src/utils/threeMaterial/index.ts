import * as THREE from "three";

var vertexShader = [
  'varying vec3	vVertexWorldPosition;',
  'varying vec3	vVertexNormal;',
  'varying vec4	vFragColor;',
  'void main(){',
  '	vVertexNormal	= normalize(normalMatrix * normal);',//将法线转换到视图坐标系中
  '	vVertexWorldPosition	= (modelMatrix * vec4(position, 1.0)).xyz;',//将顶点转换到世界坐标系中
  '	// set gl_Position',
  '	gl_Position	= projectionMatrix * modelViewMatrix * vec4(position, 1.0);',
  '}'

].join('\n');
/**
 * 大气层效果(内发光)
 * @returns {ShaderMaterial}
 */
export function AeroSphereShader(color='#ff22ff'){
  let AeroSphere = {
    uniforms: {
      coeficient: {
        type: "f",
        value: 1.0
      },
      power: {
        type: "f",
        value: 2
      },
      glowColor: {
        type: "c",
        value: new THREE.Color(color)
      }
    },
    vertexShader: vertexShader,
    fragmentShader: [
      'uniform vec3	glowColor;',
      'uniform float	coeficient;',
      'uniform float	power;',

      'varying vec3	vVertexNormal;',
      'varying vec3	vVertexWorldPosition;',

      'varying vec4	vFragColor;',

      'void main(){',
      '	vec3 worldCameraToVertex= vVertexWorldPosition - cameraPosition;',	//世界坐标系中从相机位置到顶点位置的距离
      '	vec3 viewCameraToVertex	= (viewMatrix * vec4(worldCameraToVertex, 0.0)).xyz;',//视图坐标系中从相机位置到顶点位置的距离
      '	viewCameraToVertex	= normalize(viewCameraToVertex);',//规一化
      '	float intensity		= pow(coeficient + dot(vVertexNormal, viewCameraToVertex), power);',
      '	gl_FragColor		= vec4(glowColor, intensity);',
      '}'//vVertexNormal视图坐标系中点的法向量
      //viewCameraToVertex视图坐标系中点到摄像机的距离向量
      //dot点乘得到它们的夹角的cos值
      //从中心向外面角度越来越小（从钝角到锐角）从cos函数也可以知道这个值由负变正，不透明度最终从低到高
    ].join('\n')
  }
  return new THREE.ShaderMaterial({
    uniforms: AeroSphere.uniforms,
    vertexShader: AeroSphere.vertexShader,
    fragmentShader: AeroSphere.fragmentShader,
    blending: THREE.NormalBlending,
    transparent: true,
    depthWrite:false
  });
}
/**
 * 辉光效果Grow(外发光)
 * @returns {ShaderMaterial}
 */
export function GlowSphereShader(color ='#ff22ff') {
  let GlowSphere = {
    uniforms: {
      coeficient: {
        type: "f",
        value: 0.0
      },
      power: {
        type: "f",
        value: 2
      },
      glowColor: {
        type: "c",
        value: new THREE.Color(color)
      }
    },
    vertexShader: vertexShader,
    fragmentShader: [
      'uniform vec3	glowColor;',
      'uniform float	coeficient;',
      'uniform float	power;',

      'varying vec3	vVertexNormal;',
      'varying vec3	vVertexWorldPosition;',

      'varying vec4	vFragColor;',

      'void main(){',
      '	vec3 worldVertexToCamera= cameraPosition - vVertexWorldPosition;',	//世界坐标系中顶点位置到相机位置到的距离
      '	vec3 viewCameraToVertex	= (viewMatrix * vec4(worldVertexToCamera, 0.0)).xyz;',//视图坐标系中从相机位置到顶点位置的距离
      '	viewCameraToVertex	= normalize(viewCameraToVertex);',//规一化
      '	float intensity		= coeficient + dot(vVertexNormal, viewCameraToVertex);',
      '	if(intensity > 0.65){ intensity = 0.0;}',
      '	gl_FragColor		= vec4(glowColor, intensity);',
      '}'//vVertexNormal视图坐标系中点的法向量
      //viewCameraToVertex视图坐标系中点到摄像机的距离向量
      //dot点乘得到它们的夹角的cos值
      //从中心向外面角度越来越大（从锐角到钝角）从cos函数也可以知道这个值由负变正，不透明度最终从高到低
    ].join('\n')
  }
  return new THREE.ShaderMaterial({
    uniforms: GlowSphere.uniforms,
    vertexShader: GlowSphere.vertexShader,
    fragmentShader: GlowSphere.fragmentShader,
    blending: THREE.NormalBlending,
    transparent: true,
    depthWrite:false
  });
}

/**
  * 生成并返回柱状渐变光环
  * @param {*} radius 半径
  * @param {*} height 高度
  * @param {*} color 颜色
  * @returns {ShaderMaterial}
  * @constructor
 */

//  * @returns
export function AureoleShader(geo,radius,height = 64,color) {
  //更新为BufferGeometry
    let segment = 64;
    let bottomPos = []
    let topPos = []
    let angleOffset = (Math.PI * 2) / segment
    for (var i = 0; i < segment; i++) {
      let x = Math.cos(angleOffset * i) * radius
      let z = Math.sin(angleOffset * i) * radius
      bottomPos.push(x, 0, z)
      topPos.push(x, height, z)
    }
    bottomPos = bottomPos.concat(topPos)

    let face = []
    for (var i = 0; i < segment; i++) {
      if (i != segment - 1) {
        face.push(i + segment + 1, i, i + segment)
        face.push(i, i + segment + 1, i + 1)
      } else {
        face.push(segment, i, i + segment)
        face.push(i, segment, 0)
      }
    }

  geo.setAttribute('position', new THREE.BufferAttribute(new Float32Array(bottomPos), 3))
  geo.setIndex(new THREE.BufferAttribute(new Uint16Array(face), 1))

  let c = new THREE.Color(color);
  return new THREE.ShaderMaterial({
      uniforms: {
        targetColor:{value:new THREE.Vector3(c.r,c.g,c.b)},
        height: { value: height},
      },
      side: THREE.DoubleSide,
      transparent:true,
      //depthTest:false,
      depthWrite:false,
      vertexShader: [
        "varying vec3 modelPos;",
        "void main() {",
        "   modelPos = position;",
        "	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",
        "}"
      ].join("\n"),
      fragmentShader: [
        "uniform vec3 targetColor;",
        "uniform float height;",
        "varying vec3 modelPos;",

        "void main() {",
        "   gl_FragColor = vec4(targetColor.xyz,(1.0 - modelPos.y/height)*(1.0 - modelPos.y/height));",
        "}"
      ].join("\n")
    });
}

