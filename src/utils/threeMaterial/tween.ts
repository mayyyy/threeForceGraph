import TWEEN from '@tweenjs/tween.js';
// 创建Tween动画(淡入淡出)
export function modelFadeIn(obj){
    new TWEEN.Tween(obj.material).to({ opacity: 1.0 }, 1000).start()
}
export function modelFadeOut(obj){
    new TWEEN.Tween(obj.material).to({ opacity: 0 }, 1000).start()
}
export function tagFadeIn(divTag){
  new TWEEN.Tween({opacity: 0}).to({ opacity: 1.0 }, 1000)
    .onUpdate(function (obj) {
    //动态更新div元素透明度
    divTag.style.opacity = obj.opacity;
  })
    .start()
}
export function tagFadeOut(divTag,chooseObj){
  new TWEEN.Tween({opacity: 1})
    .to({ opacity: 0 }, 400)
    .onUpdate(function (obj) {
      //动态更新div元素透明度
      divTag.style.opacity = obj.opacity;
    })
    .onComplete(function(){
      // 动画结束再从场景中移除标签
      // chooseObj.remove(divTag); //从场景移除
    })
    .start();
}
