export default {
  id: 99,
  color: "#a6cee3",
  __threeObj: {
    metadata: {
      version: 4.6,
      type: "Object",
      generator: "Object3D.toJSON"
    },
    geometries: [
      {
        uuid: "158594db-86d2-4632-8be7-a94c74ae4344",
        type: "ConeGeometry",
        radius: 9.799096249054454,
        height: 13.319934726932123,
        radialSegments: 32,
        heightSegments: 1,
        openEnded: false,
        thetaStart: 0,
        thetaLength: 6.283185307179586
      }
    ],
    materials: [
      {
        uuid: "84b5f963-b009-4b71-970b-f875be273b5d",
        type: "MeshLambertMaterial",
        color: 13296785,
        emissive: 11184810,
        reflectivity: 1,
        refractionRatio: 0.98,
        transparent: true,
        depthFunc: 3,
        depthTest: true,
        depthWrite: true,
        colorWrite: true,
        stencilWrite: false,
        stencilWriteMask: 255,
        stencilFunc: 519,
        stencilRef: 0,
        stencilFuncMask: 255,
        stencilFail: 7680,
        stencilZFail: 7680,
        stencilZPass: 7680
      }
    ],
    object: {
      uuid: "eca97ade-83be-4fc1-8677-269c043ba0df",
      type: "Mesh",
      layers: 1,
      matrix: [
        1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, -131.94521955651905,
        -321.0889447626319, 146.41267154909067, 1
      ],
      up: [0, 1, 0],
      geometry: "158594db-86d2-4632-8be7-a94c74ae4344",
      material: "84b5f963-b009-4b71-970b-f875be273b5d"
    }
  },
  index: 99,
  x: -131.94521955651905,
  y: -321.0889447626319,
  z: 146.41267154909067,
  vx: 0.000003930005767788389,
  vy: -0.000005361661125387633,
  vz: 0.000025240068688630385
};
